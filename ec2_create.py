"""
This script Manages Amazon EC2 instances using Boto3 Python SDK
"""

# Import Statements
import boto3

# Create Ec2 resource and instance name

ec2 = boto3.resource('ec2')
instance_name = 'dct-ec2-hol'

#store instance id

instance_id = None

# Check if Instance which you are trying to create already exists
# and only work with an instance that has'nt been terminated

instances = ec2.instances.all()
instance_exists = False


for instance in instances:
 if instance.tags is not None:
    for tag in instance.tags:
        if tag['Key'] == 'Name' and tag['Value'] == instance_name:
            instance_exists = True
            instance_id = instance.id
            print(f"An Instance Named '{instance_name}' with id '{instance_id}' already exists.")
            break
        if instance_exists:
            break

if not instance_exists:
    # Launch a new ec2 instance if not created already.
    new_instance = ec2.create_instances(
        ImageId='ami-04e5276ebb8451442', # Replace it with valid AMI ID
        MinCount=1,
        MaxCount=1,
        InstanceType='t2.micro',
        KeyName='dp-server',# Replace it with your key pair name
        TagSpecifications =[
            {
                'ResourceType' : 'instance',
                'Tags' : [
                    {
                        'Key' : 'Name',
                        'Value' : instance_name,
                    },
                ]
            },
         ]
        )
instance_id = new_instance[0].id
print(f"Instance Name '{instance_name}' with instance id '{instance_id}' created'")
    
    # Stop an Instance
    
    #ec2.instance(instance_id).stop()
    #print(f"Instance Name '{instance_name}' with instance id '{instance_id}' stopped)
